import Vue from 'vue'
import socketio from 'socket.io-client';
import VueSocketIO from 'vue-socket.io';
import VueGoodTable from 'vue-good-table';
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VueRouter from 'vue-router'
import App from './App.vue'
import Scanners from './Scanners.vue'

export const SocketInstance = socketio('http://192.168.22.70:32453');

Vue.use(VueMoment, {
  moment,
})
Vue.use(VueRouter);
Vue.use(VueGoodTable);
Vue.use(VueSocketIO, SocketInstance);

const routes = [
  {path: "/", component: App},
  {path: "/scanners", component: Scanners}

]
const router = new VueRouter({
  routes
})

new Vue({
  el: '#App',
  router
})
